from keymap import keymap
from option import option

#====================================================================

class menu():
	def __init__(self, coord=(0,0), size=(32,32), options=[[]]):
		'''
		Args:
			coord
			size
			options (2D list of option): arrangement of options
		'''
		self.parent=None
		self.options=options
		self.selection=options[0][0]	#XXX select top left be default... should really search for selected
		self.options[0][0].selected=True	#XXX ditto
		self.km=keymap() 				#use a seperate one for menus so people cant lock themselves out of menuing -_-

		self.__arrange_matrix()

	def __repr__(self):
		string = f"\n-------MENU-----------\n"
		string += f"parent:    {self.parent}\n"
		string += f"options:   {self.options}\n"
		string += f"selection: {self.selection}\n"
		string += f"\n-------END-MENU-----------\n"
		return string

	def input(self, key):
		pass
	def __arrange_matrix(self, min_coord=(0,0), max_coord=(64,64), hug_walls=True):
		'''arrange and link options within bounding box
		Args:
			min_coord (tuple): (y,x) coords for upper left
			max_coord (tuple): (y,x) coords for lower right
			shape (tuple):	(rows, cols) defining shape of matrix, fills till out of options
		'''
		# complain if no options
		if len(self.options)==0:
			raise IndexError
		# arrange coords
		# XXX
		# link options to neighbors
		max_r=len(self.options)
		max_c=len(self.options[0])
		for r in range(max_r):
			for c in range(max_c):
				for direction in self.km.directions:
					dr,dc=self.km.dydx(direction)
					if (r+dr) in range(0,max_r) and (c+dc) in range(0,max_c):	# if neighbor exists
						self.options[r][c].controls[direction] = self.options[r+dr][c+dc] # link to neighbor
						

	def draw(self):
		'''draws each menu option to screen'''
		for row in self.options:
			for element in row:
				print(element, end='\t')
			print()

	def run(self, key):
		'''iterates state based on key input
		'''
		try:
			action=self.selection.controls[self.km(key)]
		except KeyError:	#no action mapped
			return	
		if action is None:
			return
		elif action=="parent":
			self.parent()
			return
		elif isinstance(action, menu): # new menu	# covered by callable()
			action(self)	# pass self as parent, along with back direction?
			return
		elif callable(action):	# function
			action()
			return
		elif isinstance(action, option):	# move selection, update option.selected
			self.selection.selected=False
			self.selection=action
			self.selection.selected=True
			self.draw()
			return
		# elif self.parent is not None:	# has parent, but dont know what to do, panic and go to parent
		# 	self.parent()
		# 	return
		else:
			raise TypeError(f"action of {repr(self.selection)} not recognized")
	def __call__(self, parent=None):
		if parent is not None:	# don't overwrite parents
			self.parent=parent
		self.draw()
		key=input()
		while key in "wasdex":
			self.run(key)
			key=input()

#====================================================================

'''

def play():
	print("You pressed play, sorry there's no game yet")
def credits():
	print("This is a long list of imaginary credits\n"*25)
def quit():
	print("There very well may be no escape\n\nCallback functions anyone?")

options_menu=menu(coord=(0,0), size=(64,64), options=[
		[option("Back", action="parent", selected=True)],
		[option("A")],
		[option("B")],
		[option("C")],
		[option("D")],
		[option("E")]
		])
main_menu=menu(coord=(0,0), size=(64,64), options=[[
		option("Play", action=play),#, selected=True), 
		option("Options", action=options_menu)
	],[
		option("Credits", action=credits), 
		option("Quit", action=quit)
	]])

main_menu()

'''