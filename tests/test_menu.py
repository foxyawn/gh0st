# a mockup for a menu system

import curses 
# from curses import 

# addstr attributes:
# curses.A_BLINK
# A_BOLD
# A_DIM
# A_REVERSE
# A_STANDOUT
# A_UNDERLINE


class Selection:
    def run(self):
        raise NotImplementedError
    def next(self, input):        
        raise NotImplementedError

class Action:
    def __init__(self, action):
        self.action = action
    def __str__(self): return self.action
    def __cmp__(self, other):
        return cmp(self.action, other.action)
    # Necessary when __cmp__ or __eq__ is defined
    # in order to make this class usable as a
    # dictionary key:
    def __hash__(self):
        return hash(self.action)




class option:
	def __init__(self,parent_screen, title, h,w, y,x, selected=False):
		self.screen=parent_screen.derwin(h,w,y,x)
		self.selected=selected
		self.h=h
		self.w=w
		self.y=y
		self.x=x
		self.title=title
		#draw to screen
		self.draw()
	def toggle(self):
		if self.selected:
			self.screen.erase()
			self.screen.addstr(1,1,self.title)
			self.selected=False
		else:
			self.screen.box()
			self.selected=True
		self.screen.refresh()
	def draw(self):
		if self.selected:
			self.screen.box()
		self.screen.addstr(1,1,self.title)
		self.screen.refresh()


def main(stdscr):
	#stdscr.border(0)
	Y_max,X_max = stdscr.getmaxyx()

	menu_width=40
	menu_height=20

	menu_win = curses.newwin(menu_height, menu_width, Y_max//2-menu_height//2, X_max//2-menu_width//2)
	menu_win.box()

	menu_win.addstr(1,2,"""  _____ _    _  ____   _____ _______""")
	menu_win.addstr(2,2,""" / ____| |  | |/ __ \ / ____|__   __|""")
	menu_win.addstr(3,2,"""| |  __| |__| | | /  | (___    | |""")
	menu_win.addstr(4,2,"""| | |_ |  __  | |//| |\___ \   | |""")
	menu_win.addstr(5,2,"""| |__| | |  | |  /_| |____) |  | |""")
	menu_win.addstr(6,2,""" \_____|_|  |_|\____/|_____/   |_|""")


	play=option(menu_win,"PLAY", 3,10,8,2, selected=True)
	credits=option(menu_win,"CREDITS", 3,10,15,2)
	options=option(menu_win,"OPTIONS", 3,10,8,28)
	quit=option(menu_win,"QUIT", 3,10,15,28)



	stdscr.refresh()
	menu_win.refresh()
	# quit_win.refresh()

	key=stdscr.getch()

	# while key not in ():
	# 	quit.toggle()
		# quit_win.erase()
		# stdscr.refresh()
		# menu_win.refresh()
		# quit_win.refresh()



curses.wrapper(main)